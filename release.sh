#!/Users/romasainchuk/.nvm/versions/node/v8.17.0/bin/node

// TODO: use this as a tip to proper tool usage
const example = `npm run release-version --version='1.1.0' --message='new pull request arrived' --repository='auto-pr' --workspace='albuman' --origin='' --user='albuman' --pass='2XeHbecxZJQKaP5ssFEC'`;

// TODO: consider using .env file
const minimist = require('minimist');
const fs = require('fs');
const git = require('simple-git')({
  baseDir: process.cwd(),
});
const BitBucket = require('./bitbucket');

const optionNames = {
  version: 'version',
  msg: 'message',
  rep: 'repository',
  workSpace: 'workspace',
  origin: 'origin',
  destination: 'destination',
  usr: 'user',
  pass: 'pass'
};

const ERROR_CODES = {
  NO_PACKAGE_FILE: 4,
  INVALID_PACKAGE_FILE: 5,
  GIT_OPERATION_ERROR: 6,
  BITBUCKET_ERROR: 7
};

const { version, msg, origin, rep, workSpace, usr, pass, destination } = optionNames;
const requiredOptions = [
  usr,
  pass,
  rep,
  workSpace,
  version,
  msg,
];
const passedOptions = minimist(process.argv);

requiredOptions.forEach(option => {
  if (!passedOptions[option]) {
    throw new Error(`${option} option is not passed`);
  }
});

if (!passedOptions.destination) {
  // NOTE: destination is not mandatory, if is not specified, it will default to the repository.mainbranch === master.
  console.warn(`Destination is not specified, it will default to the 'master'`);
  passedOptions[destination] = 'master';
}

if (!passedOptions.origin) {
  // TODO: can be improved to read current branch name
  passedOptions.origin = `version_${passedOptions[version]}`;
}

let packageFile;

// TODO: consider process.exit using different codes to automate error handling by other scripts
try {
  packageFile = fs.readFileSync('./package.json');
} catch (err) {
  if (err.code === 'ENOENT') {
    console.error(`File package.json doesn't exist`);
    process.exit(ERROR_CODES.NO_PACKAGE_FILE);
  }

  console.error(err.toString());
  return;
}

let parsedPackage;

try {
  parsedPackage = JSON.parse(packageFile);
} catch (err) {
  if (err instanceof SyntaxError) {
    console.log('Invalid package.json');
    process.exit(ERROR_CODES.INVALID_PACKAGE_FILE);
  }

  console.error(err.toString());
  return;
}

if (parsedPackage.version === passedOptions[version]) {
  console.log(`Same version passed, nothing to do. Aborting...`);
  // hmmmmm, not sure about exiting code :)
  return;
}

parsedPackage.version = passedOptions[version]; // TODO: validate semver
fs.writeFileSync('./package.json', JSON.stringify(parsedPackage, null, '\t'));

const originBranch = passedOptions[origin], destinationBranch = passedOptions[destination];
const bitBucketClient = BitBucket(passedOptions[usr], passedOptions[pass]);

git
  .checkoutLocalBranch(originBranch) // TODO: handle cases when originBranch already exists or is not based on destinationBranch
  .add(['package.json'])
  .commit(passedOptions[msg])
  .push(['-u', 'origin', originBranch], async (err) => {
    if (err) {
      process.exit(ERROR_CODES.GIT_OPERATION_ERROR);
    }

    try {
      await bitBucketClient.createPullRequest({
        msg: passedOptions[msg],
        repository: passedOptions[rep],
        workSpace: passedOptions[workSpace],
        originBranch,
        destinationBranch
      });
      console.log(`Pull request successfully created`);
      process.exit(0);

    } catch (err) {
      process.exit(ERROR_CODES.BITBUCKET_ERROR);
    }
  });

//     bitBucketClient.pullrequests.list({
//       repo_slug: passedOptions[rep],
//       workspace: passedOptions[workSpace]
//     }).then(console.log);



