const { Bitbucket } = require('bitbucket');

class BitbucketClient {
    constructor(underlyingClient) {
        this.bitBucketClient = underlyingClient;
    }

    createPullRequest({ repository, workSpace, msg, originBranch, destinationBranch }) {
        return this.bitBucketClient.pullrequests.create({
            repo_slug: repository,
            workspace: workSpace,
            _body: {
                title: msg,
                source: {
                    branch: {
                        name: originBranch
                    }
                },
                destination: {
                    branch: {
                        name: destinationBranch
                    }
                }
            }
        });
    }
}

module.exports = (user, pass) => {
    return new BitbucketClient(new Bitbucket({
        auth: {
            username: user,
            password: pass,
        },
    }))
}